#include <stdio.h>
#include <stdlib.h>
/*compares if a is less then equal to or greater then. You can also right this out with if statements.
 * 	Heres another example of how you can do this function
 * 	int compare( const void *a, const void *b )
 * 	    {
 * 	    	int pa, pb;
 * 	    	pa = (int)(*(int *)a
 * 	    	pb = (int)(*(int *)b
 *
 * 	    	if(pa < pb) 
 * 	    		return -1
 * 	    	else if(pa == pb)
 * 	    		retrn 0;
 * 	    	else 
 * 	    		return 1;
 * 	    }
 */ 	    	
int compare (const void * a, const void * b)
{
	return ( *(int*)a - *(int*)b );
}

int main()
{
const int arraySize = 10;
int myarray[arraySize];
int x;
	//random seed using the system time
	srand(time(NULL));
	//add random numbers to array between 1 and 20
	for(x = 0; x < 10; x++)
		myarray[x] = 1 + rand() % 20;
	//print the data in the array
	printf("Before:\n");
	for(x = 0; x < 10; x++)
		printf("%d ",myarray[x]);

	/*qsort is in stdlib.h you pass the array, array size, size of the 
	  varible, and last but least the compare function*/
	qsort(myarray,arraySize,sizeof(int),compare);
	//print out the sorted array
	printf("\nAfter:\n");
	for(x = 0; x < arraySize; x++)
		printf("%d ",myarray[x]);
 	printf("\n");
	return 0;
}
