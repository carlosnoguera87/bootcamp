<?php

    $path = "";
    //Check if parameter come from cli
    if(count($argv)>1){
       $path = $argv[1];
    }
    //Check if parameter come from GET
    if(isset($_GET["path"])){
        $path = $_GET["path"];
    }
    
    //Scan a directory and show the result
    $directory = scandir($path);
    //Loop
    foreach($directory as $file){
        echo "\n".$file;
    }
    
?>
